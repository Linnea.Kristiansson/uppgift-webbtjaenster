package com.example.api.persons;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.HashMap;
import java.util.Map;

@Component
public class RemoteGroup {

    static Map<String, String> tokens = new HashMap<>();

    public static String getJwt(String username, String password) {
        WebClient webClient = WebClient.create("http://localhost:8081/groups/login" + username + "/" + password);

        tokens.put(username, webClient.get()
                .retrieve()
                .bodyToMono(String.class)
                .block());
        return tokens.get(username);
    }

    public static String getRemoteGroupId(String personId, String roleName, String username, String password) throws Exception {
        WebClient webClient = WebClient.create("http://localhost:8081/groups/" + personId + "/" + roleName);
        try {
            return webClient.get()
                    .headers(httpHeaders -> httpHeaders.setBearerAuth(tokens.get(username)))
                    .retrieve()
                    .bodyToMono(String.class)
                    .block();

        } catch (Exception e) {
            return webClient.get()
                    .headers(httpHeaders -> httpHeaders.setBearerAuth(getJwt(username, password)))
                    .retrieve()
                    .bodyToMono(String.class)
                    .block();
            //throw new Exception("not found");
        }
    }


}
