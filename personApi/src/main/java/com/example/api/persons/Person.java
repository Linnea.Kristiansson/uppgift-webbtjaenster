package com.example.api.persons;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Value;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Value
public class Person {
    String id;
    String name;
    String surname;
    String username;
    String password;
    List<String> groupIds;

    @JsonCreator
    public Person(@JsonProperty("name") String name,
                  @JsonProperty("surname") String surname,
                  @JsonProperty("username") String username,
                  @JsonProperty("password") String password
    ) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.password = password;
        this.groupIds = new ArrayList<>();
    }

    public void addGroupId(String groupId){
        groupIds.add(groupId);
    }
}
