package com.example.api.persons;

import com.example.api.keycloak.KeycloakConfig;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

import static org.keycloak.OAuth2Constants.USERNAME;
import static org.keycloak.common.constants.ServiceAccountConstants.CLIENT_ID;
import static org.keycloak.representations.idm.CredentialRepresentation.PASSWORD;

@Service
public class PersonService {
    List<Person> persons = new ArrayList<>();

    public List<Person> all() {
        persons.add(new Person("Maja", "Andersson", "Maja", "password"));
        persons.add(new Person("Håkan", "Håkansson", "Hakan", "password"));
        return persons;
    }

    public UserRepresentation addPerson(CreatePerson createPerson) {
        Person person = new Person(createPerson.getName(), createPerson.getSurname(), createPerson.getUsername(), createPerson.getPassword());
        persons.add(person);

        UsersResource usersResource = KeycloakConfig.getInstance().realm(KeycloakConfig.realm).users();
        UserRepresentation user = new UserRepresentation();
        user.setFirstName(person.getName());
        user.setLastName(person.getSurname());
        user.setUsername(person.getUsername());
        user.setEnabled(true);
        user.setCredentials(new ArrayList<>());

        CredentialRepresentation credential = createPasswordCredentials(person.getPassword());

        user.getCredentials().add(credential);

        try {
            System.out.println("Count: " + usersResource.count());
            Response response = usersResource.create(user);
            System.out.println("Response: " + response.getStatusInfo());
            System.out.println("Response: " + response.getStatus());
            System.out.println("Response: " + response.getMetadata());

        } catch (Exception e) {
            System.out.println(ExceptionUtils.getStackTrace(e));
            return null;
        }

        return user;
    }

    private static CredentialRepresentation createPasswordCredentials(String password) {
        CredentialRepresentation passwordCredentials = new CredentialRepresentation();
        passwordCredentials.setTemporary(false);
        passwordCredentials.setType(PASSWORD);
        passwordCredentials.setValue(password);
        return passwordCredentials;
    }

    public ResponseEntity<ResponseStatus> deletePerson(String personId) throws Exception {
        Person person = getPersonFromListById(personId);
        persons.remove(person);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    public ResponseEntity<ResponseStatus> addGroupIdToPerson(String personId, String groupName) throws Exception {
        Person person = getPersonFromListById(personId);
        person.addGroupId(RemoteGroup.getRemoteGroupId(personId, groupName, person.getUsername(), person.getPassword()));
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    public ResponseEntity<ResponseStatus> deleteGroupId(String personId, String groupId) throws Exception {
        Person person = getPersonFromListById(personId);
        person.getGroupIds().remove(groupId);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    private Person getPersonFromListById(String personId) throws Exception {
        return persons.stream().filter(p -> p.getId().equals(personId)).findFirst()
                .orElseThrow(()-> new Exception("not found"));
    }
}
