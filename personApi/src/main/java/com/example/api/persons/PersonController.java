package com.example.api.persons;

import lombok.AllArgsConstructor;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("persons")
@AllArgsConstructor
@CrossOrigin("*")
public class PersonController {
   PersonService personService;

   @GetMapping
   public List<Person> getAllPersons(){

      return personService.all();
   }

   @PostMapping("addPerson")
   public UserRepresentation addPerson(@RequestBody CreatePerson createPerson){
      return personService.addPerson(createPerson);
   }

   @DeleteMapping("deletePerson/{personId}")
   public ResponseEntity<ResponseStatus> deletePerson(@PathVariable("personId") String personId){
      try {
         return personService.deletePerson(personId);
      } catch (Exception e) {
         return ResponseEntity.badRequest().build();
      }
   }

   @DeleteMapping("/{personId}/deleteGroupId/{groupId}")
   public ResponseEntity<ResponseStatus> deleteGroupId(@PathVariable("personId") String personId,
                                                       @PathVariable("groupId") String groupId){
      try {
         return personService.deleteGroupId(personId, groupId);
      } catch (Exception e) {
         return ResponseEntity.badRequest().build();
      }

   }

   @GetMapping("/addGroupIdToPerson/{personId}/{groupName}")
   public ResponseEntity<ResponseStatus> addGroupIdToPerson(@PathVariable("personId") String personId,
                                    @PathVariable("groupName") String groupName){
      try {
         return personService.addGroupIdToPerson(personId, groupName);
      } catch (Exception e) {
         return ResponseEntity.badRequest().build();
      }
   }

}
