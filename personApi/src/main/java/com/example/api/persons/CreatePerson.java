package com.example.api.persons;

import lombok.AllArgsConstructor;
import lombok.Value;

import java.util.UUID;

@Value
public class CreatePerson {
    String name;
    String surname;
    String username;
    String password;

    public CreatePerson(String name, String surname, String username, String password) {
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.password = password;
    }
}
