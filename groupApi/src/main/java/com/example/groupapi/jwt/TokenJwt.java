package com.example.groupapi.jwt;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Value
public class TokenJwt {
    String accessToken;
    int expiresIn;
    int refreshExpiresIn;
    String refreshToken;
    String tokenType;
    int notBeforePolicy;
    String sessionState;
    String scope;

    @JsonCreator
    public TokenJwt(@JsonProperty("access_token") String accessToken,
                    @JsonProperty("expires_in") int expiresIn,
                    @JsonProperty("refresh_expires_in") int refreshExpiresIn,
                    @JsonProperty("refresh_token") String refreshToken,
                    @JsonProperty("token_type") String tokenType,
                    @JsonProperty("not_before_policy") int notBeforePolicy,
                    @JsonProperty("session_state") String sessionState,
                    @JsonProperty("scope") String scope) {
        this.accessToken = accessToken;
        this.expiresIn = expiresIn;
        this.refreshExpiresIn = refreshExpiresIn;
        this.refreshToken = refreshToken;
        this.tokenType = tokenType;
        this.notBeforePolicy = notBeforePolicy;
        this.sessionState = sessionState;
        this.scope = scope;
    }

    public static Mono<TokenJwt> acquire(String keycloakBaseUrl, String realm, String clientId, String username, String password){
        WebClient webClient = WebClient.builder()
                .baseUrl(keycloakBaseUrl)
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .build();

        return webClient.post()
                .uri("auth/realms/"+realm+"/protocol/openid-connect/token")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .body(BodyInserters.fromFormData("grant_type", "password")
                .with("client_id", clientId)
                .with("username", username)
                .with("password", password)
                .with("access_token", ""))
                .retrieve()
                .bodyToFlux(TokenJwt.class)
                .onErrorMap(e -> new Exception("failed to acquire token, " + e))
                .last();
    }

    public static String verify(String keycloakBaseUrl, String realm, String clientId, String token){
        WebClient webClient = WebClient.builder()
                .baseUrl(keycloakBaseUrl)
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .build();

        return webClient.get()
                .uri("/auth/realms/"+realm+"/protocol/openid-connect/userinfo")
                .header("Authorization", token)
                //.header("client_id", clientId)?
                .retrieve()
                .bodyToMono(String.class)
                .block();
                // .headers(http -> http.setBearerAuth(accessToken))
    }


    public static void main(String[] args) {
        TokenJwt token = acquire("http://localhost:8000/", "LinneasRealm", "linneas-client", "piff", "password")
                .block();
        System.out.println("access-token: " + token.accessToken);
    }
}
