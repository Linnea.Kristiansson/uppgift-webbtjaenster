package com.example.groupapi.groups;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Value;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
public class Group {
    String id;
    String name;
    List<String> personIds;

    @JsonCreator
    public Group(@JsonProperty("role") String name) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.personIds = new ArrayList<>();
    }

    public void addPersonId(String personId){
        personIds.add(personId);
    }
}
