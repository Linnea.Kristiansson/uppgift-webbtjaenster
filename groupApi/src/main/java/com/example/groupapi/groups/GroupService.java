package com.example.groupapi.groups;

import com.example.groupapi.jwt.TokenJwt;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseStatus;
import reactor.core.publisher.Mono;

import java.util.*;

@Service
public class GroupService {
    List<Group> groups = new ArrayList<>();

    public List<Group> all(){
        groups.add(new Group("user"));
        groups.add(new Group("admin"));
        return groups;
    }

    public ResponseEntity<ResponseStatus> addGroup(String groupName) {
        Group group = new Group(groupName);
        if(groups.contains(group)){
            return ResponseEntity.badRequest().build();
        }
        groups.add(group);
        return ResponseEntity.ok().build();
    }

    public Mono<String> getGroupIdIfTokenIsValid(String personId, String groupName, String token) throws Exception {
        Group group = getGroupFromListByName(groupName);
        System.out.println("verify metoden: " +
                TokenJwt.verify("http://localhost:8000/", "LinneasRealm", "linneas-client", token));
        group.addPersonId(personId);
        return Mono.just(group.getId());
    }

    public Group updateName(String groupId, String newGroupName) throws Exception {
        Group group = getGroupFromListById(groupId);
        group.setName(newGroupName);
        return group;
    }

    private Group getGroupFromListById(String groupId) throws Exception {
        return groups.stream().filter(g -> g.getId().equals(groupId)).findFirst()
                .orElseThrow(()-> new Exception("not found"));
    }

    private Group getGroupFromListByName(String name) throws Exception {
        return groups.stream()
                .filter(group1 -> group1.getName().equals(name))
                .findFirst()
                .orElseThrow(()-> new Exception("not found"));
    }
}
