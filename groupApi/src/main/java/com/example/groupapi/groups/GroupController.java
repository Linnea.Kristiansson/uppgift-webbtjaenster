package com.example.groupapi.groups;

import com.example.groupapi.jwt.TokenJwt;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.annotation.security.RolesAllowed;
import java.util.List;


@RestController
@RequestMapping("groups")
@AllArgsConstructor
@CrossOrigin("*")
public class GroupController {
    GroupService groupService;

    //gör om till post
    @GetMapping("/login/{username}/{password}")
    public String logIn(@PathVariable("username") String username, @PathVariable("password") String password){
        return TokenJwt.acquire("http://localhost:8000/", "LinneasRealm", "linneas-client", username, password)
                .toString();
    }

    @GetMapping
    public List<Group> getAllGroups(){
        return groupService.all();
    }

    @GetMapping("/{personId}/{groupName}")
    public Mono<String> getGroupId(@RequestHeader(value="Authorization") String bearerToken,
                                   @PathVariable("personId") String personId,
                                   @PathVariable("groupName") String groupName){
        System.out.println("token "+bearerToken);
        try {
            return groupService.getGroupIdIfTokenIsValid(personId, groupName, bearerToken);
        } catch (Exception e) {
            return Mono.just("not found");
        }
    }

    @GetMapping("updateName/{groupId}/{newGroupName}")
    public Group updateName(@PathVariable("groupId") String groupId,
                            @PathVariable("newGroupName") String newGroupName){
        try {
            return groupService.updateName(groupId, newGroupName);
        } catch (Exception e) {
            return null;
        }
    }

    @RolesAllowed("user")
    @PostMapping("/addGroup")
    public ResponseEntity<ResponseStatus> addGroup(@RequestBody String groupName){
        return groupService.addGroup(groupName);
    }

}
